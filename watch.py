#!/usr/bin/python2
from sys import exit
from subprocess import call
from threading import Thread
from time import sleep
from os import stat


class Command(Thread):
    """
    Run a shell command in its own thread
    """

    def __init__(self, cmd):
        super(Command, self).__init__()
        self.setDaemon(False)
        self.cmd = cmd

    def run(self):
        print("Executing: {0}".format(self.cmd))
        try:
            exit(call(self.cmd, shell=True) == 0)
        except KeyboardInterrupt:
            print("\b\bBye!")
        except Exception as err:
            print("Error trying to execute:\n\n{0}".format(err))


class SourceMapFixer(Thread):
    def __init__(self, source_map_path, sleep_time=3):
        super(SourceMapFixer, self).__init__()
        self.setDaemon(False)
        self.source_map_path = source_map_path
        self.sleep_time = sleep_time
        self.last_mtime = 0

    def run(self):
        while True:
            sleep(self.sleep_time)
            mtime = stat(self.source_map_path).st_mtime
            if mtime != self.last_mtime:
                src_map = open(self.source_map_path, "r").read()
                with open(self.source_map_path, "w") as source_map:
                    source_map.write(src_map.replace('"../../', '"/'))
                    self.last_mtime = stat(self.source_map_path).st_mtime


# Tuple of commands to be executed
cmd_list = (
    #Command("tsc -w --out client/assets/js/worker.js client/ts/file/worker.ts -sourcemap"),
    #Command("coffee --watch --output client/assets/js --compile  --join worker_transfer.js client/coffee/base64.coffee client/coffee/worker.coffee"),
    #Command("coffee --watch --output client/assets/js --compile --join client/coffee"),
    #Command("tsc -w --out client/assets/js/peercast.js client/ts/peercast.ts -sourcemap"),
    Command("compass watch"),
    #SourceMapFixer("client/assets/js/peercast.js.map")
)

[cmd.start() for cmd in cmd_list]
