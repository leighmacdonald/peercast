from os.path import join, dirname, abspath

# Project base directory
path_root = abspath(join(dirname(__file__)))

# Static files served over http root
path_assets = join(dirname(path_root), "client", "assets")
path_views = join(path_root, "views")

net_host = "0.0.0.0"
net_port = 8010
