from ws4py.client.tornadoclient import TornadoWebSocketClient

class WebSocketClient(WebSocket):
    def __init__(self, url="ws://localhost", trace=True, connect=True):
        self.url = url
        enableTrace(trace)
        super(WebSocketClient, self).__init__(url)
        if connect:
            self.connect(self.url)

    def connect(self, url=None):
        if not url:
            url = self.url
        if not url:
            raise AssertionError("URL cannot be empty")
        super(WebSocketClient, self).connect(url)

