from logging import getLogger
from exception import InvalidChannel
from peercast.network.request import ErrorRequest, Request
from peercast.network import broadcast
from peercast import event

log = getLogger("channel")

channels = {}

CHANNEL_PREFIX = "#"

# Name of the default channel that all users will be connected to
CHANNEL_DEFAULT = CHANNEL_PREFIX + "global"


class Channel(set):
    """
    Represents a collection of connected users
    """

    def __init__(self, channel_name, peer_limit=10):
        """ Create a new channel

        :param channel_name: Unique name of the channel
        :type channel_name: str
        :param peer_limit: Maximum peers allowed in the channel
        :type peer_limit: int
        :return:
        :rtype:
        """
        super(Channel, self).__init__()
        self.peer_limit = peer_limit
        self.name = channel_name
        self.log = getLogger("channel.{0}".format(CHANNEL_DEFAULT))
        self.log.info("Channel created")

    def join(self, peer):
        """ Add a peer to the channel if they are not currently a part of it.

        :param peer: Peer joining the channel
        :type peer: peercast.peer.Peer
        """
        if not peer in self and len(self) < self.peer_limit:
            self.add(peer)
            peer.channels.append(self)
            event.emit("event_chan_join", peer=peer, args={'channel_name': self.name})
            req_args = {
                'peer_id': peer.id,
                'channel_name': self.name,
                'msg': "{0} joined channel".format(peer.id)
            }
            self.broadcast(Request("event_chan_join", args=req_args))
            self.log.info("{0} joined".format(peer))

    def part(self, peer):
        """ Remove a peer from the channel

        :param peer:
        :type peer:
        :return:
        :rtype:
        """
        if peer in self:
            self.remove(peer)
            peer.channels.remove(self)
            req_args = {
                'peer_id': peer.id,
                'channel_name': self.name,
                'msg': "{0} left channel".format(peer.id)
            }
            self.broadcast(Request("event_chan_part", args=req_args))
            self.log.info("Peer left channel")

    def broadcast(self, request):
        """
        Send a request to all users in the channel

        :param request:
        :type request: peercast.network.request.Request
        """
        broadcast(request, self)


def join(channel_name, client):
    """
    Add a peer to a channel, if the channel doesnt exist yet it will be created

    :param channel_name: Channel name being joined
    :type channel_name: str
    :param client: Peer joining the channel
    :type client: Peer
    """
    try:
        channels[channel_name].join(client)
    except KeyError:
        chan = Channel(channel_name)
        chan.join(client)
        channels[channel_name] = chan


def get(channel_name, create=False, peer=None):
    """ Fetch a channel by its channel name. If the channel doesnt exist and the create
    parameter is true, then the channel will be created.

    :param channel_name: Channel name
    :type channel_name: str
    :return: Channel
    :rtype: Channel,Bool
    """
    try:
        chan = channels[channel_name]
    except KeyError:
        if create:
            chan = Channel(channel_name)
            channels[channel_name] = chan
        else:
            message = "Unknown channel name: {0}".format(channel_name)
            if peer:
                peer.send(ErrorRequest(message))
                return False
            else:
                raise InvalidChannel(channel_name)
    return chan


def global_chan():
    """ Fetch the global omnipresent channel

    :return: Global channel all users belong to
    :rtype: Channel
    """
    return get(CHANNEL_DEFAULT, create=True)
