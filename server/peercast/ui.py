from logging import getLogger
from jinja2 import Environment, PackageLoader

log = getLogger(__name__)
env = Environment(loader=PackageLoader("peercast", "views"))


def render(template, **kwargs):
    template = env.get_template(template)
    rendered_template = template.render(**kwargs)
    return rendered_template