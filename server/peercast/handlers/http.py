from tornado.web import RequestHandler, asynchronous
from peercast import ui


class HTTPHandler(RequestHandler):
    @asynchronous
    def get(self):
        html = ui.render("index.html")
        self.write(html)
        self.finish()


class TestHTTPHandler(RequestHandler):
    @asynchronous
    def get(self):
        html = ui.render("test.html")
        self.write(html)
        self.finish()


class FaviconHandler(RequestHandler):
    @asynchronous
    def get(self):
        self.redirect('/assets/favicon.ico')


class SourceMapHandler(RequestHandler):
    def get(self):
        html = ui.render("index.html")
        self.write(html)
        self.finish()
