"""

< connect
> connect_brd
> join global chan


"""
from logging import getLogger
from sockjs.tornado import SockJSConnection
from peercast.exception import InvalidRequest
from peercast.network.request import Request
from peercast import peer
from peercast import event
from peercast import channel


class WebSocketHandler(SockJSConnection):
    """
    Handles a WebSocket connection between the peercast server and the peer
    """

    def __init__(self, *args, **kwargs):
        super(WebSocketHandler, self).__init__(*args, **kwargs)
        self.log = getLogger("client.{0}".format(self.session.session_id))
        self.peer = peer.Peer(self)

    def on_open(self, req):
        """
        Upon opening the connection add the peer to the global peer list.

        The user will be forced to join a global channel

        :param req:
        :type req: Request
        :return:
        :rtype:
        """
        peer.add(self.peer)
        self.log.debug("Client connection established, waiting for user initiation")
        args = {
            'channel_name': channel.CHANNEL_DEFAULT,
            'forced': True
        }
        # Force the user to join the global channel
        self.peer.send(Request("event_chan_join_request", args=args))

    def on_message(self, json_data):
        """ Parse a users json request and give it to the request handler """
        try:
            request = Request.from_json(json_data)
        except InvalidRequest, err:
            self.log.error(err)
        else:
            self.handle_request(request)

    def on_close(self):
        """ Cleanly remove the peer from all the channels they were connected to. """
        peer.remove(self.peer)
        for channel in self.peer.channels:
            channel.part(self.peer)

    def handle_request(self, request):
        """
        Parse a users event request into a response and return the response if any is returned.

        :param request:
        :type request: Request
        :return:
        :rtype:
        """
        response = None
        err = None
        try:
            response = event.handle_client_request(request, self.peer)
        except AttributeError, err:
            self.log.exception("Invalid event request", err.message)
        finally:
            if err:
                response = Request("error_system")
            if response:
                self.peer.send(response)
