from logging import getLogger
from peercast.peer import connected_peers

log = getLogger("network")


def broadcast(request, peers=None):
    """ Send request to peers. If no peer list is provided the broadcast goes to all
    users currently connected

    :param peers: Peers to broadcast to
    :type peers: [Peer], None
    :param request: Request to broadcast to users
    :type request: peercast.network.request.Request
    """
    if peers is None:
        peers = connected_peers
    if not isinstance(peers, (list, tuple, set)):
        raise TypeError("Invalid peer list type")
    try:
        payload = request.encode()
    except Exception:
        log.exception("Failed to encode request payload")
    else:
        if payload:
            for peer in peers:
                peer.send(payload)
