from json import dumps, loads
from peercast.exception import InvalidRequest


class Request(object):
    def __init__(self, event="", args=None):
        self.event = event
        self.args_real = args if args else {}

    def encode(self):
        return dumps({"event": self.event, "args": self.args})

    def set(self, argument_key, value):
        self.args_real[argument_key] = value

    @property
    def args(self):
        return self.args_real

    @classmethod
    def from_json(cls, json_data):
        try:
            json_obj = loads(json_data)
            req = Request(event=json_obj['event'], args=json_obj['args'])
        except Exception as err:
            raise InvalidRequest(err)
        else:
            return req


class ErrorRequest(Request):
    """
    Usually returned to the requesting peer upon a error processing a event request
    """

    def __init__(self, msg=None):
        super(ErrorRequest, self).__init__("event_request_error")
        if msg:
            self.args['msg'] = msg
