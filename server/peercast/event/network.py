from time import time
from logging import getLogger
from peercast.network.request import Request
from peercast import channel
from peercast.event import event

log = getLogger("event.network")


@event
def event_connection(peer=None):
    """
    User connected

    :type peer: Peer
    """
    request = Request("event_connection", args={'id': peer.id, 'msg': "Client connected ({0})".format(peer)})
    channel.global_chan().broadcast(request)


@event
def event_disconnect(peer=None):
    """
    User disconnection

    :type peer: Peer
    """
    log.debug("Client disconnected ({0})".format(peer))
    #request = Request("event_disconnect", args={'id': peer.id, })
    #channel.global_chan().broadcast(request)


#@event
#def event_ping(peer=None):
#   channel.global_chan().broadcast(Request("event_ping", args={'time': time(), 'msg': "Ping!"}))
