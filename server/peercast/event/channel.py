from peercast.network.request import Request
from peercast import channel
from peercast.event import event


@event
def event_chan_join(peer=None, channel_name=None):
    """
    User requests to join a channel

    :param peer:
    :type peer:
    :param channel_name:
    :type channel_name:
    :return:
    :rtype:
    """
    channel.get(channel_name=channel_name, create=True, peer=peer).join(peer)


@event
def event_chan_part(peer=None, channel_name=None, msg=None):
    """
    User initiated request to part a channel

    :param peer:
    :type peer:
    :param channel_name:
    :type channel_name:
    :param msg:
    :type msg:
    :return:
    :rtype:
    """
    pass


@event
def event_chan_msg(peer=None, channel_name=None, msg=None):
    """
    Send a message to all peers in a channel

    :param peer:
    :type peer:
    :param channel_name:
    :type channel_name:
    :param msg:
    :type msg:
    :return:
    :rtype:
    """
    request = Request("event_chan_msg", args={"channel_name": channel_name, "peer_id": peer.id, "msg": msg})
    channel.get(channel_name=channel_name, create=False, peer=peer).broadcast(request)


@event
def event_chan_attendees(peer=None, channel_name=None):
    chan = channel.get(channel_name)
    args = {
        "channel_name": channel_name,
        "attendees": [peer.id for peer in chan]
    }
    return Request("event_chan_attendees", args=args)
