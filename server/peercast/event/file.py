"""
Offer request chain

1>> Offer files to peer (event_file_offer)
2<< Peer accepts or rejects offer (event_file_offer_accept, event_file_offer_reject)
1>> If rejected, offer is wiped from requests queues
1>> If accepted, peer begins transfer of file queue

"""
from logging import getLogger
from peercast.event import event
from peercast.network.request import Request
from peercast.peer import get_peer_by_id

log = getLogger("event.file")

pending_offers = {}
accepted_offers = {}

def add_offer(offer):
    pending_offers[offer['offer_id']] = offer
    log.debug("Added new file offer to queue")


def accept_offer(offer):
    real_offer = pending_offers[offer['offer_id']]
    del pending_offers[offer['offer_id']]
    accepted_offers[real_offer['offer_id']] = real_offer
    log.debug("File offer accepted")


@event
def event_file_offer(peer=None, peer_id=None, file_info=None, offer_id=None):
    log.debug("got event_file_offer")
    if peer.id == peer_id:
        # Don't send ourselves files
        return None
    offer = {"files": file_info, "peer_id": peer.id, 'offer_id': offer_id}
    request = Request("event_file_offer_request", offer)
    get_peer_by_id(peer_id).send(request)
    add_offer(offer)


@event
def event_file_offer_accept(peer=None, offer_id=None):
    offer = pending_offers[offer_id]
    accept_offer(offer)
    request = Request("event_file_offer_start", {'offer_id': offer_id})
    get_peer_by_id(offer['peer_id']).send(request)


@event
def event_file_offer_reject(peer=None, offer_id=None):
    pass
