from __builtin__ import callable
from logging import getLogger
from functools import wraps
from os import listdir
from os.path import dirname

from peercast.network.request import Request, ErrorRequest
from peercast.exception import EventError, InvalidChannel, InvalidPeer, PeerCastError


RELAY_PREFIX = "relay_"
EVENT_PREFIX = "event_"

log = getLogger("event")


class EventRegistry(dict):
    """
    A dict like object which holds the registered event handlers for each event type
    """

    def register_handler(self, event, handler):
        """
        Register a handler inside the registry
        """
        if not event in self:
            self[event] = handler
        else:
            if self[event] is handler:
                return False
        log.debug("Registered event handler: {0} <- {1}".format(event, handler))
        return True


registry = None
if not registry:
    # Only start 1 registry in case of being imported several times
    registry = EventRegistry()


def module_list():
    """
    Generate and return a list of event modules names under the current directory.

    :return: Event modules
    :rtype: basestring[]
    """
    return [m.split(".")[0] for m in listdir(dirname(__file__)) if not m.startswith("__") and m.endswith(".py")]


def register_handlers():
    """ Register all the event handlers listed in the module list """
    for module_name in module_list():
        mod = __import__('peercast.event.{0}'.format(module_name), fromlist=[module_name])
        for fn in filter(lambda f: f.startswith(EVENT_PREFIX), mod.__dict__.keys()):
            registry.register_handler(fn, getattr(mod, fn))


def handle_relay_request(request, peer=None, relay_args_key="relay_args"):
    """
    Handle relaying requests to other peers from a peer.

    The event must start with the relay_ prefix. If you want to pass in arguments then you can use
    the replay_args key in your arguments dict to set the values you want. All values under that key
    will be forwarded.

    The forwarded request will have the relay_ prefix exchanged for the event_ prefix.
    The request args will be merged into the new request args dict.

    :param request: Network event request from the peer
    :type request: Request
    :param peer: Peer making the request
    :type peer: Peer
    :param relay_args_key: Key of the args dict to pass onto the relay request
    :type relay_args_key: basestring
    """
    from peercast.peer import get_peer_by_id
    from peercast import channel
    from peercast.network import broadcast

    log.debug("Got relay req: {0}".format(request))
    args = {}
    if relay_args_key in request.args:
        args.update(request.args[relay_args_key])

    args['peer_id'] = peer.id
    event_name = "_".join([u"event"] + request.event.split("_")[1:])
    relay_req = Request(event_name, args=args)
    if 'peers' in request.args:
        broadcast(relay_req, get_peer_by_id(request.args['peers']))
    else:
        channel.global_chan().broadcast(relay_req)


def handle_client_request(request, peer):
    """
    Parse a client provided Request into an event and execute it

    :param request:
    :type request: Request
    :param peer:
    :type peer: peercast.peer.Peer
    :return:
    :rtype: None, Request
    """
    response = None
    error = None
    try:
        if request.event.startswith(RELAY_PREFIX):
            return handle_relay_request(request, peer)
        else:
            handler = registry[request.event]
    except KeyError:
        raise EventError("No handler registered under the name: {0}".format(request.event))
    except (AttributeError, EventError) as err:
        log.exception("Invalid event request", err)
        error = err
    else:
        response = emit(handler, peer=peer, args=request.args)
    finally:
        if error:
            response = Request("event_error", args={'msg': "Error processing request"})
        if isinstance(response, Request):
            return response


def emit(fn=None, peer=None, args=None):
    """
    Emit an event call

    :param fn:
    :type fn:
    :param peer:
    :type peer:
    :param args:
    :type args:
    :return:
    :rtype:
    """
    if not callable(fn):
        try:
            fn = registry[fn]
        except KeyError:
            log.error("Tried executing unregistered event: {0}".format(fn))
            return False
    resp = None
    if not args:
        args = {}
    try:
        resp = fn(peer=peer, **args)
    except TypeError as err:
        log.exception(err)
    except Exception as err:
        log.exception("Error trying to execute event function: {0}".format(fn.__name__))
    finally:
        return resp


def event(fn):
    """
    Event decorator which provides some automatic error handling of event handler exceptions.

    :param fn:
    :type fn: callable
    :return:
    :rtype:
    """

    @wraps(fn)
    def wrapped_event(*args, **kwargs):
        request = None
        try:
            request = fn(*args, **kwargs)
        except (InvalidPeer, InvalidChannel) as error:
            request = ErrorRequest(error.message)
        except PeerCastError:
            error = "Uncaught exception processing event: {0}".format(fn.__name__)
            log.exception(error)
            request = ErrorRequest
        finally:
            if request and "peer" in kwargs:
                kwargs['peer'].send(request)

    return wrapped_event
