from logging import getLogger
from peercast.network.request import Request
from peercast.network import broadcast
from peercast.event import event

log = getLogger("event.messaging")


@event
def event_msg(msg=None, peer=None):
    if msg:
        broadcast(Request("event_msg", args={'msg': msg}))
