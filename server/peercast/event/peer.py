from hashlib import sha1
from base64 import decodestring
from logging import getLogger
from time import time
from peercast.network.request import Request
from peercast.peer import connected_peers
from peercast.channel import global_chan
from peercast.event import event

log = getLogger("event.peer")


@event
def event_peer_list(peer=None):
    request = Request("event_peer_list")
    request.args['peer_list'] = [{'id': c.id, 'connected': c.connected_time} for c in connected_peers]
    return request


@event
def event_peer_msg(peer=None, target_peer=None, msg=None):
    pass


@event
def event_peer_name(peer=None, name=None):
    """
    Update a peers name to a new friendly name

    :param peer:
    :type peer: Peer
    :param name:
    :type name: basestring
    """
    pass


@event
def event_peer_send_file(peer=None, payload=None, target_peer=None, sha_hash=None):
    file_name = "file_{0}".format(time())
    data_hash = sha1(payload).hexdigest()
    decoded_data = decodestring(payload)
    if data_hash == sha_hash:
        with open(file_name, 'wb') as fp:
            fp.write(decoded_data)
        log.debug("wrote payload file: {0}".format(file_name))
    else:
        log.error("Got invalid hash!")


@event
def event_peer_search(peer=None, search_term=None):
    log.debug("Got search req: {0}".format(search_term))
    if search_term:
        matched_peers = [p.id for p in global_chan() if p.id.lower().startswith(search_term) and p.id != peer.id]
        ret_args = {'matched_peers': matched_peers, 'msg': ','.join(matched_peers)}
        return Request("event_peer_search_results", args=ret_args)
