from time import time
from logging import getLogger
from peercast import event
from peercast.exception import InvalidPeer

log = getLogger("peer")

connected_peers = set()


def get_peer_by_id(peer_id):
    """
    Fetch a list of connected peers based on their ID.

    If peer_id is a list then a list of peers will be returned
    If peer_id is a string then just the peer will be returned

    :param peer_id:
    :type peer_id: basestring, string[]
    :return:
    :rtype: Peer[], Peer
    """
    peer_list = not isinstance(peer_id, basestring)
    peers = []
    for peer in connected_peers:
        if peer.id in peer_id:
            if peer_list:
                peers.append(peer)
            else:
                return peer
    if not peers:
        raise InvalidPeer(peer_id)
    return peers


def add(peer):
    """
    This establishes the user as connected and "in the matrix"

    :param peer: Peer
    """
    connected_peers.add(peer)
    event.emit("event_connection", peer=peer)


def remove(peer):
    """

    :param peer:
    :return:
    """
    connected_peers.remove(peer)
    event.emit("event_disconnect", peer=peer)


class Peer(object):
    def __init__(self, connection):
        """

        :param connection:
        :type connection: peercast.handlers.websocket.WebSocketHandler
        :return:
        """
        self.connection = connection
        self.time_start = time()
        self.channels = []

    def send(self, request):
        """ Send and optionally encode a request to the user

        :param request:
        :type request: Request
        :return:
        """
        payload = request.encode() if hasattr(request, 'encode') else request
        try:
            self.connection.send(payload)
        except Exception as err:
            log.exception("Failed to send payload to peer", err)
        else:
            log.debug("Sent request to peer")

    @property
    def connected_time(self):
        return int(time() - self.time_start)

    @property
    def id(self):
        return self.connection.session.session_id

    def __str__(self):
        return self.id

    def disconnect(self):
        pass
