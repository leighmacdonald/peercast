class PeerCastError(Exception):
    pass


class InvalidRequest(PeerCastError):
    pass


class EventError(PeerCastError):
    pass


class InvalidChannel(InvalidRequest):
    def __init__(self, channel_name):
        self.channel = channel_name
        self.message = "Invalid channel requested: {0}".format(channel_name)

class InvalidPeer(InvalidRequest):
    def __init__(self, peer_id=None):
        self.peer_id = peer_id
        self.message = "Invalid peer requested: {0}".format(peer_id)
