#!../virtenv/bin/python
from logging import getLogger, DEBUG
from os.path import dirname, abspath, join
from tornado.web import Application, StaticFileHandler
from tornado.ioloop import IOLoop, PeriodicCallback
from sockjs.tornado import SockJSRouter

log = getLogger()
log.setLevel(DEBUG)
log.info("""
                               _
 ___ ___ ___ ___ ___ ___   ___| |_
| . | -_| -_|  _|  _| .'|_|_ -|  _|
|  _|___|___|_| |___|__,|_|___|_|
|_|
            Pew Pew
""")

from peercast.handlers.http import HTTPHandler, FaviconHandler, TestHTTPHandler
from peercast.handlers.websocket import WebSocketHandler
from peercast import event

import config

event.register_handlers()

# Setup route handlers
handlers = [
    (r"^/", HTTPHandler),
    (r"^/test", TestHTTPHandler), # For testerings
    (r"^/(.*\.html)", StaticFileHandler, {'path': abspath(join(dirname(__file__), "peercast", "views"))}),
    (r"^/coffee/(.*)", StaticFileHandler, {'path': abspath(join(dirname(dirname(__file__)), "client", "coffee"))}),
    (r"^/assets/(.*)", StaticFileHandler, {'path': config.path_assets}),
    (r"^/favicon.ico", FaviconHandler)
]

io_loop = IOLoop.instance()

callbacks = set()

ws_handler = SockJSRouter(WebSocketHandler, "/ws")

# Init global timer callbacks
callbacks.add(PeriodicCallback(lambda: event.emit("event_ping"), 50000))

[cb.start() for cb in callbacks]

application = Application(handlers + ws_handler.urls, debug=True)
application.listen(port=config.net_port, address=config.net_host)
try:
    io_loop.start()
except KeyError:
    log.info("Caught kill request, goodbye cruel world...")
except Exception as err:
    log.error(err)
