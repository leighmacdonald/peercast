# PeerCast

For peercasting!~

## Setup

Requires python 2.7. 3.x works but there are a couple modifications to fix for 3.3 support, will move to 3.3 later.

    cd /project/path/root
    virtualenv virtenv
    source virtenv/bin/activate
    pip install -r requirements.txt

    npm install -g typescript
    ./watch_ts.sh &
    ./peercastd.py

Typescript 0.8.3 is required.


Premade Typescript Interfaces are avail @ https://github.com/borisyankov/DefinitelyTyped
