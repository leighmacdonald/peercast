(function () {
    var chunk_size, prepare_chunks;
    var __bind = function (fn, me) {
        return function () {
            return fn.apply(me, arguments);
        };
    };
    chunk_size = 1024 * 1000;
    prepare_chunks = function (file, chunks_idx) {
        var ch, chunk_idx, chunk_total, i, reader, _results;
        if (chunks_idx) {
            chunk_total = chunks_idx.length;
        } else {
            chunk_total = Math.ceil(file.size / chunk_size);
        }
        i = 0;
        _results = [];
        while (i < chunk_total) {
            chunk_idx = chunks_idx ? chunks_idx[i] : i;
            reader = new FileReader();
            (function (chunk_idx) {
                return reader.onload = __bind(function (event) {
                    var chunk, file_chunk;
                    chunk = event.target.result;
                    file_chunk = {
                        chunk_idx: chunk_idx,
                        payload: chunk,
                        hash: "HASH"
                    };
                    return this.postMessage({
                        cmd: "chunk_ready",
                        args: {
                            file: file,
                            file_chunk: file_chunk
                        }
                    });
                }, this);
            })(chunk_idx);
            ch = file.content.slice(chunk_idx * chunk_size, chunk_size);
            reader.readAsArrayBuffer(ch);
            _results.push(i++);
        }
        return _results;
    };
    addEventListener('message', function (event) {
        var args, data;
        data = event.data;
        args = data.args;
        switch (data.cmd) {
            case "start":
                return this.postMessage('Got Start', void 0);
            case "stop":
                return this.close();
            case "prepare_chunks":
                return prepare_chunks(args.file, args.chunks_idx);
            default:
                return this.postMessage('Unknown command', void 0);
        }
    }, false);
}).call(this);
