#chunk_size = 1024 * 1000; # 1mb
#
#prepare_chunks = (file, chunks_idx) ->
#  if chunks_idx
#    chunk_total = chunks_idx.length
#  else
#    chunk_total = Math.ceil(file.size / chunk_size)
#  i = 0
#  while i < chunk_total
#    chunk_idx = if chunks_idx then chunks_idx[i] else i
#    reader = new FileReader()
#    do (chunk_idx) ->
#      reader.onload = (event) =>
#        chunk = event.target.result
#        file_chunk =
#          chunk_idx: chunk_idx
#          payload: Base64.encode64(chunk)
#          hash: "HASH"
#        #encode.sha1(chunk)
#
#        this.postMessage({cmd: "chunk_ready", args:
#          {file: file, file_chunk: file_chunk}})
#
#    ch = file.content.slice(
#      chunk_idx * chunk_size,
#      chunk_size
#    )
#    reader.readAsArrayBuffer(ch)
#    i++
#
#file_system = undefined
#
#get_file_system = () ->
#  if file_system
#    return file_system
#  else
#    fs_type = PERSISTENT
#    fs_size = 10 * 1024 * 1024
#    webkitStorageInfo.requestQuota(fs_type, fs_size, (gb) ->
#      webkitRequestFileSystem(fs_type, gb, (fs) ->
#        file_system = fs
#      )
#    )
#
#read_file = (file_id, file_content) ->
#  reader = new FileReader()
#  reader.onload = (event) =>
#    payload = event.target.result
#    #payload = Base64.encode64(payload)
#    this.postMessage({cmd: "file_read", args: {file_id: file_id, file_content: payload}})
#  reader.readAsArrayBuffer(file_content)
#
#write_file = (file, file_content) ->
#  file_content = Base64.decode64(file_content)
#  file_system = get_file_system()
#  file_system.root.getFile(file.name, {create: true}, (file_entry) ->
#    file_entry.createWriter((writer) ->
#      writer.write(file_content)
#      this.postMessage({cmd: "file_written", args: {file_id: file.file_id}})
#    )
#  )
#
#str2ab = (str) ->
#  buf = new ArrayBuffer(str.length) # 2 bytes for each char
#  buf_view = new DataView(buf)
#  for char in str
#    buf_view[char] = str.charCodeAt(char)
#  return buf_view
#
#
#addEventListener('message', (event) ->
#  data = event.data
#  args = data.args
#  switch data.cmd
#    when "start"
#      this.postMessage('Got Start', undefined)
#    when "stop"
#      this.close()
#    when "prepare_chunks"
#      prepare_chunks(args.file, args.chunks_idx)
#    when "read_file"
#      read_file(args.file_id, args.file_content)
#    when "write_file"
#      #write_file(args.file, args.file_content)
#      this.postMessage({cmd: "file_written", args: {file_id: args.file.file_id, file_content: str2ab(Base64.decode64(args.file_content))}})
#    else
#      this.postMessage('Unknown command', undefined)
#, false)
#
#
#
#
