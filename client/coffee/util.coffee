namespace "pc.util", (exports) ->

    # Generate a unique id
    #
    # int length
    exports.generate_unique_id = (length = 20) ->
        charset = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
        return _(length).times(() =>
            charset.charAt(Math.floor(Math.random() * charset.length))).join("")