namespace "pc.peer", (exports) ->

    class exports.Peer

        #
        id: null

        #
        name: null

        #
        constructor: (@id, @name = "Anonymous") ->

        #
        toString: () ->
            return @id

