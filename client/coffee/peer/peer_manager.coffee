namespace "pc.peer", (exports) ->

    Peer = pc.peer.Peer

    class exports.PeerManager

        # Angular RootScope
        root_scope: null

        #
        peers: null

        #
        constructor: (@root_scope) ->
            @peers = {}

        #
        get_peer: (peer_id, name) ->
            if not peer_id?
                return @peers
            peer = @peers[peer_id]
            if peer not instanceof Peer
                peer = new Peer(peer_id, name)
                @peers[peer.id] = peer
            return peer

        #
        remove_peer: (peer_id) ->
            delete @peers[peer_id]

        #
        exclude: (excluded_peers, subset_peers) ->
            excluded_peers = if _.isArray(excluded_peers) then excluded_peers else [excluded_peers]
            peers = subset_peers or @peers
            filtered_peers = _.difference(_.values(peers), excluded_peers)
            return filtered_peers