namespace "pc.directive", (exports) ->
    Request = pc.network.Request

    exports.file_transfer = () ->
        templateUrl: 'file_transfer.html'
        replace: true
        restrict: 'E'
        scope: true
        link: ($scope, $elt, $attrs, $ctrl) ->
            $elt.bind 'change', (event) ->
                $scope.s_file.files_to_offer = event.target.files
        controller: ($scope, $rootScope, file_mgr) ->

            # namespace
            $scope.s_file = {}

            #
            $scope.s_file.file_mgr = file_mgr

            #
            $scope.s_file.transports = {}


            #
            on_message = (msg) ->
                msg = msg.data or msg
                if angular.isString(msg)
                    req = Request.parse_request(msg)
                    event = req.event
                    args = req.args
                else
                    event = "event_files_blob"
                    args = {}
                    args.file_content = msg
                $rootScope.safe_apply(
                    () =>
                        $scope.$broadcast(event, args)
                    $scope
                )


            # shortcut for the room_id
            room_id = $scope.room.room_id


            #
            $scope.s_file.get_transports = () ->
                $scope.s_file.file_mgr.add_transport(
                    room_id
                    $scope.s_room.get_transports("file", $scope.s_file, on_message)
                )


            #
            $scope.s_file.offer_files = () ->
                file_mgr.offer_files($scope.s_file.files_to_offer, $scope.room.get_peers(true))
                $scope.s_file.files_to_offer = null


            #
            $scope.s_file.accept_file = (file_id) ->
                file_mgr.accept_files(file_id)


            #
            $scope.s_file.decline_file = (file_id) ->
                file_mgr.decline_files(file_id)


            #
            $scope.s_file.get_uploads = () ->
                return file_mgr.get_uploads(room_id)


            #
            $scope.s_file.get_downloads = () ->
                return file_mgr.get_downloads(room_id)


            # Listen the new connections to open the DataChannels
            $scope.$on "event_room_new_connection", (event, args) ->
                $rootScope.$log.log("File: A new connection has been added to the room.")
                $scope.s_file.get_transports()


            #

            $scope.$on "event_files_offer", (event, args) ->
                $rootScope.$log.info(
                    "Files transfer requested (#{args.files.length} Files) from peer : #{room_id}"
                )
                file_mgr.add_download(args.files)


            #

            $scope.$on "event_files_accept", (event, args) ->
                $rootScope.$log.info(
                    "Files accepted (#{args.file_ids.length} Files) from peer : #{room_id}"
                )
                file_mgr.prepare_file(args.file_ids[0])


            #

            $scope.$on "event_files_decline", (event, args) ->
                $rootScope.$log.info(
                    "Files declined (#{args.file_ids.length} Files) from peer : #{room_id}"
                )
                file_mgr.handle_decline(args.file_ids[0])


            #

            $scope.$on "event_files_ack", (event, args) ->
                $rootScope.$log.info("File ACK received from peer : #{room_id}")
                file_mgr.handle_ack(args.file_id)


            #

            $scope.$on "event_files_blob", (event, args) ->
                $rootScope.$log.info("File Blob received => from peer : #{room_id}")
                file_mgr.handle_blob(room_id, args.file_content)


            #

            $scope.$on "event_files_current_transfer", (event, args) ->
                $rootScope.$log.info("Current transfer ID received")
                file_mgr.set_current_transfer(args.file_id)


            #

            $scope.$on "event_file_end_chunk", (event, args) ->
                $rootScope.$log.info("File end of chunks (file ID " + args.file_id + ") from peer : " + room_id)