namespace "pc.directive", (exports) ->

    Request = pc.network.Request
    Chat = pc.chat.Chat

    exports.chat = () ->
        templateUrl: 'chat.html'
        replace: true
        restrict: 'E'
        scope: true
        link: ($scope, $element, $attrs, $ctrl) ->
            $scope.$watch(
                's_chat.chat.messages.length'
                () ->
                    new_message = $scope.s_chat.chat.get_messages(true)
                    if new_message
                        date = new Date(new_message.timestamp).toLocaleTimeString()
                        container = $element.find(".chat_output_container")
                        new_output = container.find(".chat_output:first").clone()
                        new_output.removeClass("template")
                        new_output.find(".chat_time").html(date)
                        new_output.find(".chat_from").html(new_message.from)
                        new_output.find(".chat_message").html(new_message.message)
                        container.append(new_output)
                true
            )
        controller: ($scope, $rootScope, $element, $attrs) ->

            # namespace
            $scope.s_chat = {}

            #
            $scope.s_chat.chat = new Chat($rootScope)

            #
            $scope.s_chat.transports = {}


            # DataChannel onmessage handler
            on_message = (msg, from) ->
                msg = msg.data or msg
                req = Request.parse_request(msg)
                $rootScope.safe_apply(
                    () =>
                        $scope.$broadcast(req.event, req.args, from)
                    $scope
                )
                return


            #
            $scope.s_chat.get_transports = () ->
                $scope.s_chat.chat.add_transport($scope.s_room.get_transports("chat", $scope.s_chat, on_message))


            #
            $scope.s_chat.send_msg = () ->
                @chat.send_message(@msg)
                @msg = ''
                return


            # Listen the new connections to open the DataChannels
            $scope.$on "event_room_new_connection", (event, args) ->
                $rootScope.$log.log("Chat: A new connection has been added to the room.")
                $scope.s_chat.get_transports()

            #
            $scope.$on "event_chat_msg", (event, args, from) ->
                $rootScope.$log.log("Chat: Received message...")
                $scope.s_chat.chat.handle_message(args.message, from)