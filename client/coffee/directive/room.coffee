namespace "pc.directive", (exports) ->

    Util = pc.util

    exports.room = () ->
        templateUrl: 'room.html'
        replace: true
        restrict: 'E'
        scope:
            room: '='
        link: ($scope, $element, $attrs, $ctrl) ->
        controller: ($scope, $rootScope, $attrs, connection_mgr) ->

            #namespace
            $scope.s_room = {}

            $scope.s_room.connections = {}

            # Return a Connection for each peer who joined the room
            $scope.s_room.get_connections = () ->
                return $scope.s_room.connections


            # Return a DataChannel per Connection
            $scope.s_room.get_transports = (transport_label, scope, on_message) ->
                for peer_id, connection of $scope.s_room.get_connections()
                    if scope.transports[peer_id] is undefined
                        data_channel = connection.get_data_channel("#{transport_label}_#{$scope.room.room_id}")
                        data_channel.then((dc) ->
                            $rootScope.$log.log("Room: Setting data channel handler")
                            dc.set_handler("message", (msg) -> on_message(msg, peer_id))
                        )
                        scope.transports[peer_id] = data_channel
                return scope.transports


            #
            $scope.s_room.add_attendee = () ->
                all_peers = (label:"#{peer.name} (#{peer.id})", value: peer.id for peer_id, peer of $rootScope.peer_mgr.get_peer())
                choose_notif = $rootScope.notifier.choose("Select the attendee you want to add to the room:", all_peers, true)
                choose_notif.then((notification) ->
                    $scope.room.invite(peer_id) for peer_id in notification.answer
                    return
                )


            # Handle a room join. Only the admin of the room receive this request
            $scope.$on "event_room_join", (event, args) ->
                if args.room_id is $scope.room.room_id
                    $scope.room.ack_join(args.peer_id)


            # Handle a room join ack.
            $scope.$on "event_room_join_ack", (event, args) ->
                if args.room_id is $scope.room.room_id
                    new_attendee = args.new_attendee
                    $scope.room.add_attendee(new_attendee.peer_id, true, new_attendee.index)
                    # if it does not concern myself
                    if new_attendee.peer_id isnt $rootScope.me.id
                        # if I joined the room before the peer acknowledged, I'll be considered as initiator of the connection
                        if $scope.room.joined() and 0 < $scope.room.index() < $scope.room.index(new_attendee.peer_id)
                            $rootScope.$log.log("Room: Added a new connection to #{new_attendee.peer_id} as initiator in the room #{args.room_id}.")
                            $scope.s_room.connections[new_attendee.peer_id] = connection_mgr.initiate(new_attendee.peer_id)
                        # if I joined the room after the peer acknowledged, he'll be considered as initiator of the connection
                        else if $scope.room.joined()
                            $rootScope.$log.log("Room: Added a new connection to #{new_attendee.peer_id} in the room #{$scope.room.room_id}.")
                            $scope.s_room.connections[new_attendee.peer_id] = connection_mgr.get_connection(new_attendee.peer_id)
                    else
                        # if the ack concern myself, I add all the peers who joined the room before me
                        for attendee in args.attendees_joined
                            $scope.room.add_attendee(attendee.peer_id, true, attendee.index)
                            $rootScope.$log.log("Room: Added a new connection to #{attendee.peer_id} in the room #{$scope.room.room_id}.")
                            $scope.s_room.connections[attendee.peer_id] = connection_mgr.get_connection(attendee.peer_id)

                    $scope.$broadcast("event_room_new_connection")







