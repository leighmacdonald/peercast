namespace "pc.directive", (exports) ->
    exports.visio = () ->
        templateUrl: 'visio.html'
        replace: true
        restrict: 'E'
        scope: true
        link: ($scope, $element, $attrs, $ctrl) ->
            $scope.$watch(
                'remote_stream'
                (new_val, old_val) ->
                    $element.find('video.remote_stream')[0].play()
                true
            )
        controller: ($scope, $element, $attrs) ->
            $scope.peerConnection.rtc_pc.onaddstream = (event) ->
                $scope.remote_stream = event.stream
                $scope.remote_blobURL = URL.createObjectURL(event.stream)
            #$scope.peerConnection.add_stream($scope.$root.local_stream)

            $scope.start_visio = () ->
                $scope.peerConnection.add_stream($scope.$root.local_stream)

            $scope.stop_visio = () ->
                $scope.peerConnection.remove_stream($scope.$root.local_stream)
