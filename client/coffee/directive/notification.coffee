namespace "pc.directive", (exports) ->

    exports.notification = () ->
        templateUrl: 'notification.html'
        replace: true
        restrict: 'E'
        scope:
            notification: '='
        link: ($scope, $element, $attrs) ->
            if $scope.notification.modal
                $element.foundation("reveal", "open")
                $element.on("closed", () ->
                    $scope.$root.safe_apply(
                        () ->
                            $scope.notification.resolve()
                        $scope
                    )
                )
                $scope.$on "$destroy", () ->
                    #$element.foundation("reveal", "close")
                    # little hack for reveal because Foundation 4 has a lot of bugs
                    jQuery(".reveal-modal-bg").remove()


        controller: ($scope, $rootScope) ->

            # namespace
            $scope.s_notif = {}

            $scope.s_notif.resolve = (answer) ->
                $scope.notification.resolve(answer)