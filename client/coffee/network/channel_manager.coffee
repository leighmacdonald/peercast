namespace "pc.network", (exports) ->

    Channel = pc.network.Channel

    class exports.ChannelManager

        # Angular RootScope
        root_scope: null

        # Channels the user is a member of
        channels: null

        #
        constructor: (@root_scope) ->
            @channels = {}

        # Return the channels
        get_channels: () ->
            return @channels


        # Fetch and return the channel matching the name provided.
        get_channel: (channel_name) ->
            channel = @channels[channel_name]
            if not channel
                @root_scope.$log.log("Creating new channel")
                channel = new Channel(@root_scope, channel_name)
                @channels[channel_name] = channel
            return channel