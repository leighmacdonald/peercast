namespace 'pc.network', (exports) ->

    Request = pc.network.Request
    DataChannel = pc.network.DataChannel

    class exports.Connection
    # Angular RootScope
        root_scope: null

        #
        peer_id: null

        # True if the peer who created the connection is the initiator
        initiator: null

        # MediaConstraints
        constraints: null

        # Built-in RTCPeerConnection
        rtc_pc: null

        # Collection of DataChannel
        data_channels: null

        #
        negotiation_needed: null

        #
        negotiating: null

        constructor: (@root_scope, @peer_id, @initiator = false, url_stun = "stun:stun.l.google.com:19302") ->
            this.negotiating = false
            this.negotiation_needed = false
            this.data_channels = {}

            servers =
                iceServers: [
                    url: url_stun
                ]
            @constraints =
                mandatory:
                    OfferToReceiveAudio: true
                    OfferToReceiveVideo: true
                optional: [
                    RtpDataChannels: true
                ]


            @rtc_pc = new webkitRTCPeerConnection(servers, @constraints);

            # RTCPeerConnection events handlers
            @rtc_pc.onicecandidate = @on_ice_candidate
            @rtc_pc.onnegotiationneeded = @on_negotiation_needed
            @rtc_pc.onconnecting = @on_connecting
            @rtc_pc.onopen = @on_open
            @rtc_pc.onaddstream = @on_add_stream
            @rtc_pc.onremovestream = @on_remove_stream
            @rtc_pc.onstatechange = @on_state_change
            @rtc_pc.onsignalingstatechange = @on_signaling_state_change
            @rtc_pc.onicechange = @on_ice_change
            @rtc_pc.onidentityresult = @on_identity_result
            @rtc_pc.onicechange = @on_ice_change
            @rtc_pc.ondatachannel = @on_data_channel


        # Return the status of the RTCPeerConnection.

        get_status: () ->
            return @rtc_pc.signalingState


        # Create an offer and send it to the peer.

        create_offer: () ->
            @rtc_pc.createOffer(
                (local_sdp) =>
                    @root_scope.$log.log("Created offer to " + @peer_id)
                    @set_bandwidth(local_sdp)
                    @rtc_pc.setLocalDescription(
                        local_sdp
                        () =>
                            @root_scope.$log.log("Set local SDP => success. Send local SDP to " + @peer_id)
                            @root_scope.ws.send(
                                new Request(
                                    "relay_peer_connect_offer",
                                    peers: [@peer_id]
                                    relay_args:
                                        remote_sdp: local_sdp
                                )
                            )
                        @error_callback
                    )
                @error_callback
                @constraints
            )


        # Receive a answer from the peer with its SDP and set it

        process_answer: (remote_sdp) ->
            @root_scope.$log.log("Received answer from #{@peer_id}. Set remote SDP")
            @rtc_pc.setRemoteDescription(
                new RTCSessionDescription(remote_sdp)
                () =>
                    @root_scope.$log.log("Set remote SDP => success")
                @error_callback
            )


        # Receive a offer from the peer with its SDP and set it.

        create_answer: (remote_sdp) ->
            @root_scope.$log.log("Receive offer and remote SDP from #{@peer_id}. Set remote SDP")
            @rtc_pc.setRemoteDescription(
                new RTCSessionDescription(remote_sdp)
                () =>
                    @root_scope.$log.log("Set remote SDP => success")
                    @rtc_pc.createAnswer(
                        (local_sdp) =>
                            @root_scope.$log.log("Create answer to #{@peer_id}")
                            @set_bandwidth(local_sdp)
                            @rtc_pc.setLocalDescription(
                                local_sdp
                                () =>
                                    @root_scope.$log.log("Set local SDP => success. Send local SDP to #{@peer_id}")
                                    @root_scope.ws.send(
                                        new Request(
                                            "relay_peer_connect_answer",
                                            peers: [@peer_id]
                                            relay_args:
                                                remote_sdp: local_sdp
                                        )
                                    )
                                @error_callback
                            )
                        @error_callback
                        @constraints
                    )
                @error_callback
            )


        # Add a remote ICE candidate

        add_remote_ice_candidate: (remote_ice_candidate) ->
            @root_scope.$log.log("Add remote ICE candidate from #{@peer_id}")
            @rtc_pc.addIceCandidate(new RTCIceCandidate(remote_ice_candidate))


        # Error callback for all the RTCPeerConnection methods

        error_callback: (error_msg) =>
            @root_scope.$log.error(error_msg)
            throw new Error(error_msg)


        # Create and/or return a DataChannel instance

        get_data_channel: (label) ->
            dc = @data_channels[label]
            if dc is undefined
                deferred = @root_scope.$q.defer()
                if @initiator
                    rtc_dc = @rtc_pc.createDataChannel(label, {reliable: false})
                    dc = new DataChannel(@root_scope, deferred, rtc_dc)
                else
                    dc = new DataChannel(@root_scope, deferred)
                @data_channels[label] = dc
            return dc.deferred.promise


        # Add a MediaStream to the RTCPeerConnection

        add_stream: (stream) ->
            @rtc_pc.addStream(stream)


        # Remove a MediaStream to the RTCPeerConnection

        remove_stream: (stream) ->
            @rtc_pc.removeStream(stream)


        #

        set_bandwidth: (sdp, bandwidth = 102400) ->
            parts = sdp.sdp.split("b=AS:30")
            replace = "b=AS:#{bandwidth}"
            # 100 Mbps
            sdp.sdp = parts[0] + replace + parts[1]


        #

        on_ice_candidate: (event) =>
            if event.candidate
                @root_scope.$log.log("Send local ICE candidate to #{@peer_id}")
                @root_scope.ws.send(
                    new Request(
                        "relay_peer_connect_ice_candidate",
                        peers: [@peer_id]
                        relay_args:
                            remote_ice_candidate: event.candidate
                    )
                )


        #

        on_negotiation_needed: (event) =>
            @root_scope.$log.log(event.type)
            if not @negotiating
                @negotiating = true
                @create_offer()
            else
                @negotiation_needed = true


        #

        on_connecting: (event) =>
            @root_scope.$log.log(event.type)


        #

        on_open: (event) =>
            @root_scope.$log.log(event.type)
            @negotiating = false
            if @negotiation_needed
                @negotiation_needed = false
                @create_offer()


        #

        on_add_stream: (event) =>
            @root_scope.$log.log(event.type)


        #

        on_remove_stream: (event) =>
            @root_scope.$log.log(event.type)


        #

        on_state_change: (event) =>
            @root_scope.$log.log(event.type)


        #

        on_signaling_state_change: (event) =>
            @root_scope.$log.log(event.type, event.target.signalingState)
            if @get_status() is "stable"
                @negotiating = false
                if @negotiation_needed
                    @negotiation_needed = false
                    @create_offer()


        #

        on_ice_change: (event) =>
            @root_scope.$log.log(event.type)


        #

        on_identity_result: (event) =>
            @root_scope.$log.log(event.type)


        #

        on_data_channel: (event) =>
            @root_scope.$log.log(event.type)
            data_channel = @data_channels[event.channel.label]
            if data_channel is undefined
                @get_data_channel(event.channel.label)
                data_channel = @data_channels[event.channel.label]
            data_channel.set_rtc_dc(event.channel)
