namespace "pc.network", (exports) ->

    Connection = pc.network.Connection

    class exports.ConnectionManager

        # Angular RootScope
        root_scope: null

        #
        connections: null


        constructor: (@root_scope) ->
            this.connections = {}

        #
        get_connections: () ->
            return @connections


        # Create and/or return a Connection instance

        get_connection: (peer_id, initiator = false) ->
            peer_connection = @connections[peer_id]
            if peer_connection instanceof Connection
                return peer_connection
            else
                peer_connection = new Connection(@root_scope, peer_id, initiator)
                @connections[peer_id] = peer_connection
                return peer_connection


        # Initiate a Connection

        initiate: (peer_id) ->
            @get_connection(peer_id, true)


        # Answer to an offer

        answer: (peer_id, offer) ->
            peer_connection = @get_connection(peer_id)
            try
                peer_connection.create_answer(offer)
            catch error
                throw new Error(error)


        # Conclude the Connection if the peer sent an answer

        conclude: (peer_id, answer) ->
            peer_connection = @get_connection(peer_id)
            try
                peer_connection.process_answer(answer)
            catch error
                throw new Error(error)


        # Process the remote ICE candidate received

        add_ice_candidate: (peer_id, remote_ice_candidate) ->
            peer_connection = @get_connection(peer_id)
            try
                peer_connection.add_remote_ice_candidate(remote_ice_candidate)
            catch error
                throw new Error(error)