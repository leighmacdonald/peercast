namespace "pc.network", (exports) ->

    Request = pc.network.Request

    class exports.DataChannel

    # Angular RootScope

        root_scope: null

        # A deferred to handle the asynchronous datachannel opening

        deferred: null

        # Built-in RTCDataChannel

        rtc_dc: null

        # Reliable Instance

        reliable: null

        constructor: (@root_scope, @deferred, rtc_dc) ->
            if rtc_dc
                @set_rtc_dc(rtc_dc)


        # Initialize the event handlers

        init_handlers: () ->
            @rtc_dc.onopen = @on_open
            @rtc_dc.onclose = @on_close
            @reliable.onmessage = @on_message
            @rtc_dc.onerror = @on_error


        # Return the status of the DataChannel

        get_status: () ->
            if @rtc_dc
                return @rtc_dc.readyState


        # Set the RTCDataChannel instance

        set_rtc_dc: (rtc_dc) ->
            @rtc_dc = rtc_dc
            @reliable = new Reliable(@rtc_dc, true)
            @init_handlers()



        #

        send: (msg) ->
            if msg instanceof Request
                msg = msg.encode()
            if @rtc_dc
                @reliable.send(msg)


        #

        on_open: (event) =>
            @root_scope.$log.log("DataChannel opened")
            @root_scope.safe_apply(() =>
                @deferred.resolve(@)
            )


        #

        on_message: (event) =>
            @root_scope.$log.log("From DataChannel: #{event.data}")


        #

        on_error: (event) =>
            @root_scope.$log.log("DataChannel error")
            throw new Error(event.data)


        #

        on_close: (event) =>
            @root_scope.$log.log("DataChannel closed")


        # Give the ability to override a handler to customize the process

        set_handler: (handler, fn) ->
            if @rtc_dc
                handler_name = "on" + handler
                @reliable[handler_name] = fn
            else
                handler_name = "on_" + handler
                this[handler_name] = fn
