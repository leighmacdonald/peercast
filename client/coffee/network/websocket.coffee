namespace 'pc.network', (exports) ->

    Request = pc.network.Request

    class exports.WebSocketClient

        # Angular RootScope
        root_scope: null

        constructor: (@root_scope, address = '//' + window.location.host + '/ws') ->
            @sock_js = new SockJS(address)
            @sock_js.onopen = @on_open
            @sock_js.onmessage = @on_message
            @sock_js.onclose = @on_close

        set_session_id: () ->
            url_split = @sock_js._transport.url.split("/")
            @session_id = url_split[url_split.length - 2]
            @root_scope.$log.log("Session ID: " + @session_id)

        on_open: () =>
            @set_session_id()
            @root_scope.$broadcast('event_ws_opened')
            @root_scope.$log.info('WebSocket opened.')

        on_message: (event) =>
            try
                req = Request.parse_request(event.data)
            catch error
                @root_scope.$log.log("Error: " + error)
                return false

            if req.args['msg'] isnt undefined
                @root_scope.$log.info('event: ' + req.event + " msg: " + req.args['msg'])

            @root_scope.safe_apply(
                () =>
                    @root_scope.$broadcast(req.event, req.args)
            )
            return true

        on_close: () =>
            @root_scope.$log.info('WebSocket closed.')
            @root_scope.$broadcast("event_ws_closed")

        send: (request, callback) ->
            @sock_js.send(request.encode())

        close: () ->
            @sock_js.close()