namespace "pc.network", (exports) ->

    class exports.Request

        #
        constructor: (@event, @args = {}) ->

        #
        encode: () ->
            try
                return JSON.stringify(
                    event: @event
                    args: @args
                )

            catch error
                return JSON.stringify(
                    event: "event_request_error"
                    msg: error
                )


        #

        @parse_request: (json_data) ->
            json_obj = JSON.parse(json_data)
            return new @(json_obj.event, json_obj.args)