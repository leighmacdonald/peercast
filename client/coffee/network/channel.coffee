namespace "pc.network", (exports) ->

    Request = pc.network.Request

    class exports.Channel

    # Angular RootScope
        root_scope: null

        #
        name: ""

        #
        attendees: null

        #
        joined: false


        constructor: (@root_scope, @name) ->
            @attendees = {}
            @joined = false


        # Return the attendees currently in the channel

        get_attendees: () ->
            return @attendees


        # Return the total number of attendees currently in the channel

        size: () ->
            return _.size(@attendees)


        # Check if the peer exists in the channel

        attendee_exists: (peer_id) ->
            if @attendees[peer_id] then true else false


        # Request the server to join the channel

        join: () ->
            if @joined
                return false
            @root_scope.ws.send(new Request("event_chan_join", {channel_name: @name}))
            @root_scope.$log.log("You requested to join #{@name}")


        # Add an attendee to the channel

        add_attendee: (peer_id) ->
            if @attendee_exists(peer_id)
                return
            @attendees[peer_id] =
                peer_id: peer_id
            if peer_id is @root_scope.me.id
                @joined = true
                @fetch_attendees()
                msg = "You have joined the channel!"
            else
                msg = "#{peer_id} has joined the channel"
            @root_scope.$log.log("#{@name} :: #{msg}")


        # Remove a user from the channel

        part: (peer_id) ->
            delete @attendees[peer_id]
            if peer_id is @root_scope.me.id
                @joined = false
            @root_scope.$log.log("Removed #{peer_id} from channel #{@name}")


        # Fetch the peers who joined the channel

        fetch_attendees: () ->
            @root_scope.ws.send(new Request("event_chan_attendees", {channel_name: @name}))


        # Broadcast a message to the channel

        broadcast: (msg) ->
            @root_scope.ws.send(new Request("event_chan_msg", {channel_name: @name, msg: msg}))
