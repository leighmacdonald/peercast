namespace "pc.room", (exports) ->

    Util = pc.util
    Room = pc.room.Room

    class exports.RoomManager

    # Angular RootScope
        root_scope: null

        # Set of rooms
        rooms: null

        #
        constructor: (@root_scope) ->
            @rooms = {}

        #
        get_room: (room_id, admin) ->
            room = @rooms[room_id]
            if not room
                room = new Room(@root_scope, room_id, admin)
                @rooms[room.room_id] = room
            return room
