namespace "pc.room", (exports) ->

    Util = pc.util
    Request = pc.network.Request

    class exports.Room

        # Angular RootScope
        root_scope: null

        # Unique Id
        room_id: null

        # Set of attendees
        attendees: null

        #
        admin: null


        #
        constructor: (@root_scope, @room_id, @admin) ->
            @attendees = {}
            if not @room_id
                @room_id = Util.generate_unique_id()
                @admin = @root_scope.me.id
            @add_attendee(@admin, true, 1)


        #
        attendee_exists: (peer_id) ->
            return _.contains(_.pluck(@attendees, "peer_id"), peer_id)

        #
        join: () ->
            @root_scope.ws.send(
                new Request(
                    "relay_room_join"
                    peers: [@admin]
                    relay_args:
                        room_id: @room_id
                        peer_id: @root_scope.me.id
                )
            )

        #
        ack_join: (peer_id) ->
            if not @is_admin()
                return false

            attendees_joined = @get_attendees(true)
            index = attendees_joined.length + 1
            new_attendee = @add_attendee(peer_id, true, index)
            attendees_joined.push(new_attendee)

            @root_scope.ws.send(
                new Request(
                    "relay_room_join_ack"
                    peers: _.pluck(attendees_joined, "peer_id")
                    relay_args:
                        room_id: @room_id
                        new_attendee: new_attendee
                        attendees_joined: attendees_joined
                )
            )


        #
        add_attendee: (peer_id, joined = false, index = 0) ->
            @attendees[peer_id] =
                peer_id: peer_id
                joined: joined
                index: index


        #
        invite: (peer_id) ->
            if peer_id is @root_scope.me.id or not @is_admin() or @attendee_exists(peer_id)
                return false

            @add_attendee(peer_id)

            @root_scope.ws.send(
                new Request(
                    "relay_room_invite"
                    peers: [peer_id]
                    relay_args:
                        room:
                            room_id: @room_id
                            admin: @admin
                )
            )

        #
        is_admin: (peer_id) ->
            peer_id = peer_id or @root_scope.me.id
            return peer_id is @admin

        #
        joined: (peer_id) ->
            peer_id = peer_id or @root_scope.me.id
            return @attendees[peer_id].joined

        #
        index: (peer_id) ->
            peer_id = peer_id or @root_scope.me.id
            return @attendees[peer_id].index

        #
        get_attendees: (joined = false) ->
            if joined
                attendees = _.filter(@attendees, (attendee) => return @joined(attendee.peer_id))
            else
                attendees = @attendees
            return attendees