namespace "pc.chat", (exports) ->

    class exports.ChatManager

        # Angular RootScope
        root_scope: null

        # Set of chat
        chats: null

        #
        constructor: (@root_scope) ->
            @chats = {}


        # Return a new or an existing Chat instance
        #
        # string peer_id
        get_chat: (peer_id) ->
            chat = @chats[peer_id]
            if not chat
                chat = new Chat(@root_scope)
                @chats[peer_id] = chat
            return chat


