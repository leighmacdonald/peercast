namespace "pc.chat", (exports) ->

    Request = pc.network.Request

    class exports.Chat

        # Angular RootScope
        root_scope: null

        # Set of chat
        messages: null

        # Set of transport interfaces
        transports: null

        #
        constructor: (@root_scope) ->
            @messages = []
            @transports = {}

        #
        send_message: (message) ->
            @send(
                "event_chat_msg"
                message: message
            )
            @handle_message(message, @root_scope.me.id)

        #
        handle_message: (message, from) ->
            if @messages.length >= 10
                @messages = []
            @messages.push(
                message: message
                from: from
                timestamp: Date.now()
            )

        #
        get_messages: (last = false) ->
            if last
                return @messages[@messages.length - 1]
            else
                return @messages

        #
        add_transport: (transports) ->
            for peer_id, transport of transports
                @transports[peer_id] = transport

        #
        send: (event, args) ->
            request = new Request(event, args)
            for peer_id, transport of @transports
                transport.then((dc) =>
                    dc.send(request)
                )
