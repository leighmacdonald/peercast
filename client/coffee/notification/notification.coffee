namespace "pc.notification", (exports) ->

    class exports.UserNotification

        # Message
        message: null

        # Deferred instance to handle the asynchronous confirmation
        deferred: null

        # Answer of the user
        answer: null

        # Boolean either the notification is displayed as a modal window or not
        modal: null

        #
        constructor: (@message, @deferred, @modal = false) ->
            @answer = null


    class exports.ConfirmNotification extends exports.UserNotification

        #
        resolve: (@answer) ->
            if @answer
                @deferred.resolve(@)
            else
                @deferred.reject(@)


    class exports.OpenNotification extends exports.UserNotification

        #
        resolve: (@answer) ->
            if @answer isnt ""
                @deferred.resolve(@)
            else
                @deferred.reject(@)


    class exports.ChooseNotification extends exports.UserNotification

        # Set of pre-defined choices
        choices: null

        #
        constructor: (message, deferred, @choices = [], modal = false) ->
            super(message, deferred, modal)

        #
        resolve: (@answer) ->
            if @answer and @answer.length
                @deferred.resolve(@)
            else
                @deferred.reject(@)
