namespace "pc.notification", (exports) ->

    UserNotification = pc.notification.UserNotification
    ConfirmNotification = pc.notification.ConfirmNotification
    OpenNotification = pc.notification.OpenNotification
    ChooseNotification = pc.notification.ChooseNotification

    class exports.Notifier

        # Angular RootScope
        root_scope: null

        # Set of notifications
        notifications: null

        #
        constructor: (@root_scope, @notifications = []) ->

        #
        defer: () ->
            deferred = @root_scope.$q.defer()
            deferred.promise.then(
                (notification) =>
                    @delete_notification(notification)
                (notification) =>
                    @delete_notification(notification)
            )
            return deferred

        #
        delete_notification: (notification) ->
            for notif, i in @notifications
                if notif is notification
                    @notifications.splice(i, 1)
                    break
            return

        # Display a message to the user
        notify: (message) ->
            deferred = @defer()
            @notifications.push(new UserNotification(message, deferred))
            return deferred.promise

        # Ask a question to the user answerable by Yes or No
        confirm: (message, modal = false) ->
            deferred = @defer()
            @notifications.push(new ConfirmNotification(message, deferred, modal))
            return deferred.promise

        # Ask an open question to the user, every answer are accepted
        ask: (message) ->
            deferred = @defer()
            @notifications.push(new OpenNotification(message, deferred))
            return deferred.promise

        # Ask a question to the user answerable by predefined values
        choose: (message, choices = [], modal = false) ->
            deferred = @defer()
            @notifications.push(new ChooseNotification(message, deferred, choices, modal))
            return deferred.promise

        #
        get_notifications: () ->
            return @notifications

