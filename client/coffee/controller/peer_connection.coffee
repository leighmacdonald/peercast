namespace "pc.controller", (exports) ->
    exports.peer_connection_controller = ($rootScope, $scope, connection_mgr) ->

        #====================== Data ==========================
        $scope.peer_connections = connection_mgr.get_connections()

        #==================== Handlers =========================


        # Answer to an offer

        $scope.$on "event_peer_connect_offer", (event, args) ->
            peer = $rootScope.peer_mgr.get_peer(args.peer_id)
            connection_mgr.answer(peer, args.remote_sdp)


        # Conclude the PeerConnection if the peer sent an answer

        $scope.$on "event_peer_connect_answer", (event, args) ->
            peer = $rootScope.peer_mgr.get_peer(args.peer_id)
            connection_mgr.conclude(peer, args.remote_sdp)


        # Process the remote ICE candidate received

        $scope.$on "event_peer_connect_ice_candidate", (event, args) ->
            peer = $rootScope.peer_mgr.get_peer(args.peer_id)
            connection_mgr.add_ice_candidate(peer, args.remote_ice_candidate)


        #================== Functions =========================


#====================== Filters ====================================
