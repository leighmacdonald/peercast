namespace "pc.controller", (exports) ->

    Request = pc.network.Request
    Room = pc.room.Room

    exports.channel_controller = ($rootScope, $scope, channel_mgr) ->

        #====================== Data ==========================
        $scope.s_channel = {}
        # kind of namespace
        $scope.s_channel.channels = channel_mgr.get_channels()

        #==================== Handlers =========================


        # Handle a invitation to a channel from the server. If the force argument is set the client will be forced
        # to join the channel.
        $scope.$on 'event_chan_join_request', (event, args) ->
            if args.forced
                channel = channel_mgr.get_channel(args.channel_name)
                channel.join()
                $scope.$log.log("Forces to join: #{args.channel_name}")
            else
                $scope.$log.log("Join channel request received: #{args.channel_name}")


        # Handle a channel join event request from the server.
        $scope.$on 'event_chan_join', (event, args) ->
            channel = channel_mgr.get_channel(args.channel_name)
            $rootScope.peer_mgr.get_peer(args.peer_id)
            channel.add_attendee(args.peer_id)
            $scope.s_channel.write_output(channel, "#{args.peer_id} has joined the channel.")


        # Handle a channel part request from the server
        $scope.$on 'event_chan_part', (event, args) ->
            channel = channel_mgr.get_channel(args.channel_name)
            channel.part(args.peer_id)
            $rootScope.peer_mgr.remove_peer(args.peer_id)
            $scope.s_channel.write_output(channel, "#{args.peer_id} has parted the channel.")


        #
        $scope.$on 'event_chan_msg', (event, args) ->
            channel = channel_mgr.get_channel(args.channel_name)
            $scope.s_channel.write_output(channel, "#{args.peer_id} says : #{args.msg}")


        #
        $scope.$on 'event_chan_attendees', (event, args) ->
            channel = channel_mgr.get_channel(args.channel_name)
            $rootScope.peer_mgr.get_peer(peer_id) and channel.add_attendee(peer_id) for peer_id in args.attendees
            return



        #================== Functions =========================


        #

        $scope.s_channel.write_output = (channel, msg) ->
            if not channel.output
                channel.output = ''
            now = new Date()
            channel.output += "#{now.toTimeString()} || #{msg} \n"


        #

        $scope.s_channel.join = () ->
            channel = channel_mgr.get_channel($scope.s_channel.channel_to_join)
            channel.join()


        #

        $scope.s_channel.send = (channel, msg) ->
            channel.broadcast(msg)
            msg = ""

        #

        $scope.s_channel.create_room = (selected_attendees) ->
            $rootScope.$broadcast(
                "event_room_create"
                attendees: selected_attendees
            )


        #
        $scope.s_channel.get_attendees = (channel) ->
            toto = _.filter(channel.get_attendees(), (attendee) -> attendee.peer_id isnt $rootScope.me.id)
            return toto