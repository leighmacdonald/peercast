namespace "pc.controller", (exports) ->

    exports.room_controller = ($rootScope, $scope, room_mgr) ->

        #====================== Data ==========================
        $scope.room_mgr = room_mgr

        #==================== Handlers =========================


        # Handle a room creation
        $scope.$on "event_room_create", (event, args) ->
            room = room_mgr.get_room()
            room.invite(attendee.peer_id) for attendee in args.attendees
            return

        # Handle a room invitation
        $scope.$on "event_room_invite", (event, args) ->
            confirm = $rootScope.notifier.confirm("Do you want to join the room #{args.room.room_id} ?")
            confirm.then(
                (notif) =>
                    room = room_mgr.get_room(args.room.room_id, args.room.admin)
                    room.join()
            )


#====================== Filters ====================================
