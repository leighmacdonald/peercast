namespace "pc.file", (exports) ->

    Util = pc.util

    FileStatus =
        PENDING: 'Pending'
        ACCEPTED: 'Accepted'
        DECLINED: 'Declined'
        IN_TRANSFER: 'In Transfer'
        COMPLETE: 'Complete'

    exports.FileStatus = FileStatus


    class exports.FileInstance

        #
        id: null

        #
        name: null

        #
        size: null

        #
        type: null

        #
        last_modified_date: null

        #
        status: null

        #
        content: null


        #
        constructor: (@name, @size, @type, @last_modified_date, @content) ->
            @id = Util.generate_unique_id()
            @status = FileStatus.PENDING

        #
        @from_input: (input_files) ->
            files = _.map(input_files, (f) =>
                return new @(f.name, f.size, f.type, f.lastModifiedDate, f)
            )
            return files
