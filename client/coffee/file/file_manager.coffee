namespace "pc.file", (exports) ->

    Request = pc.network.Request
    Util = pc.util
    FileInstance = pc.file.FileInstance
    FileStatus = pc.file.FileStatus


    class exports.Manager

        # Angular RootScope
        root_scope: null

        # Set of transport interfaces
        transports: null

        #
        file_transfer: null

        #

        queue_up: null

        #

        queue_dn: null

        #

        worker_up: null

        #

        worker_dn: null

        #

        file_system: null

        constructor: (@root_scope) ->
            @transports = {}
            @queue_up = {}
            @queue_dn = {}
        #@file_transfer = new FileTransfer(@root_scope, @transports)
        #@start_workers()


        #

        start_workers: () ->
            @worker_dn = new Worker("/assets/js/worker_transfer.js")
            @worker_dn.addEventListener("message", @worker_handler.bind(this))
            @worker_up = new Worker("/assets/js/worker_transfer.js")
            @worker_up.addEventListener("message", @worker_handler.bind(this))


        #

        worker_handler: (event) ->
            data = event.data
            args = data.args
            switch data.cmd
                when "file_read"
                    file = @get_files_by_id(args.file_id)[0]
                    @send(file.to, args.file_content)
                when "file_written"
                    file = @get_files_by_id(args.file_id)[0]
                    file_content = args.file_content
                    @root_scope.$log.info("File saved.")
                    fs_type = 'PERSISTENT'
                    fs_size = 10 * 1024 * 1024

                    window.webkitStorageInfo.requestQuota(fs_type, fs_size, (gb) =>
                        window.webkitRequestFileSystem(fs_type, gb, (fs) =>
                            file_system = fs
                            file_system.root.getFile(file.name, {create: true}, (file_entry)  =>
                                file_entry.createWriter((writer) =>
                                    writer.write(new Blob([file_content], {type: file.type}))
                                )
                            )
                        )
                    )
                else
                    @root_scope.$log.info("Unknown Command")


        #

        add_transport: (room_id, transports) ->
            # For the files transfer, the transports are sorted by room
            # as there is only one FileManager for the whole application
            @transports[room_id] = {} if not @transports[room_id]?
            for peer_id, transport of transports
                @transports[room_id][peer_id] = transport
            return


        #

        get_files_by_id: (file_ids) ->
            file_ids = if _.isArray(file_ids) then file_ids else [file_ids]
            files = _.map(file_ids, (file_id) =>
                file = if @queue_dn[file_id] then @queue_dn[file_id] else @queue_up[file_id]
                return file
            )
            return files


        #

        get_uploads: (room_id) ->
            if room_id
                return _.filter(@queue_up, (upload) =>
                    return upload.to == room_id
                )
            else
                return @queue_up


        #

        get_downloads: (room_id) ->
            if room_id
                return _.filter(@queue_dn, (download) =>
                    return download.to == room_id
                )
            else
                return @queue_dn


        #

        add_upload: (files, peers) ->
            files = if _.isArray(files) then files else [files]
            _.each(files, (file) =>
                @queue_up[file.id] =
                    file: file
                    recipients:
                        for peer_id in peers
                            peer_id: peer_id
                            status: FileStatus.PENDING
            )
            return @queue_up


        #

        add_download: (files) ->
            files = if _.isArray(files) then files else [files]
            _.each(files, (file) =>
                @queue_dn[file.file_id] = file
            )
            return @queue_dn


        #

        set_files_status: (file_ids, status) ->
            file_ids = if _.isArray(file_ids) then file_ids else [file_ids]
            _.each(@get_files_by_id(file_ids), (file) =>
                file.status = status
            )


        #

        offer_files: (input_files, peers) ->
            files = FileInstance.from_input(input_files)
            @add_upload(files)
            @send(
                peers
                new Request(
                    "event_files_offer"
                    files: _.map(files, (file) =>
                        return _.omit(file, "content")
                    )
                )
            )


        #

        send_file: (file) ->
            file.status = FileStatus.IN_TRANSFER
            @send(file.to, new Request("event_files_current_transfer", {file_id: file.file_id}))
            @send(file.to, file.content)


        #

        set_current_transfer: (file_id) ->
            @set_files_status(file_id, FileStatus.IN_TRANSFER)


        #

        prepare_file: (file_id) ->
            file = @get_files_by_id(file_id)[0]
            file.status = FileStatus.ACCEPTED
            next_transfer = @get_next_transfer()
            if next_transfer
                @send_file(next_transfer)


        #

        handle_blob: (room_id, file_content) ->
            downloads = @get_downloads(room_id)
            current_download = _.find(downloads, (download) =>
                return download.status == FileStatus.IN_TRANSFER
            )
            current_download.status = FileStatus.COMPLETE
            @send(room_id, new Request("event_files_ack", {file_id: current_download.file_id}))
            @save_file(current_download, file_content)


        #

        get_file_system: () ->
            deferred = @root_scope.$q.defer()
            if @file_system
                @root_scope.safe_apply(() =>
                    deferred.resolve()
                )
            else
                fs_type = 'PERSISTENT'
                fs_size = 10 * 1024 * 1024
                window.webkitStorageInfo.requestQuota(fs_type, fs_size, (gb) =>
                    window.webkitRequestFileSystem(fs_type, gb, (fs) =>
                        @file_system = fs
                        @root_scope.safe_apply(() =>
                            deferred.resolve()
                        )
                    )
                )
            return deferred.promise


        #

        save_file: (file, file_content) ->
            @get_file_system().then((result) =>
                @file_system.root.getFile(file.name, {create: true}, (file_entry) =>
                    file_entry.createWriter((writer) =>
                        file_content = new Uint8Array(file_content)
                        writer.write(new Blob([file_content], {type: file.type}))
                    )
                )
            )


        #

        handle_ack: (file_id) ->
            file = @get_files_by_id(file_id)[0]
            file.status = FileStatus.COMPLETE
            next_transfer = @get_next_transfer()
            if next_transfer
                @send_file(next_transfer)


        #

        get_next_transfer: () ->
            uploads = @get_uploads()
            in_transfer = _.find(uploads, (upload) =>
                return upload.status == FileStatus.IN_TRANSFER
            )
            if in_transfer
                return false
            else
                return _.find(uploads, (upload) =>
                    return upload.status == FileStatus.ACCEPTED
                )


        # Accept a list of files

        accept_files: (file_ids) ->
            file_ids = if _.isArray(file_ids) then file_ids else [file_ids]
            files = @get_files_by_id(file_ids)
            for file in files
                file.status = FileStatus.ACCEPTED
            @send(files[0].to, new Request("event_files_accept", {file_ids: file_ids}))


        # Decline one or several files

        decline_files: (file_ids) ->
            file_ids = if _.isArray(file_ids) then file_ids else [file_ids]
            files = @get_files_by_id(file_ids)
            for file in files
                file.status = FileStatus.DECLINED
            @send(files[0].to, new Request("event_files_decline", {file_ids: file_ids}))


        # Handle a decline response

        handle_decline: (file_ids) ->
            file_ids = if _.isArray(file_ids) then file_ids else [file_ids]
            files = @get_files_by_id(file_ids)
            for file in files
                file.status = FileStatus.DECLINED


        #
        send: (room_id, event, args) ->
            request = if event and args then new Request(event, args) else event
            transports = @transports[room_id]
            for peer_id, transport of transports
                transport.then((dc) =>
                    dc.send(request)
                )
