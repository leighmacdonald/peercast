#_require config.coffee
#_require util.coffee

#_require peer/peer.coffee
#_require peer/peer_manager.coffee

#_require network/request.coffee
#_require network/websocket.coffee
#_require network/data_channel.coffee
#_require network/peer_connection.coffee
#_require network/peer_connection_manager.coffee
#_require network/channel.coffee
#_require network/channel_manager.coffee

#_require notification/notification.coffee
#_require notification/notifier.coffee

#_require file/file.coffee
#_require file/file_manager.coffee

#_require room/room.coffee
#_require room/room_manager.coffee

#_require chat/chat.coffee

#_require controller/main.coffee
#_require controller/channel.coffee
#_require controller/peer_connection.coffee
#_require controller/room.coffee

#_require directive/room.coffee
#_require directive/chat.coffee
#_require directive/visio.coffee
#_require directive/file_transfer.coffee


namespace 'pc', (exports) ->
    exports.init = () ->
        angular
            .module('PeerCast', [])
            .config(($interpolateProvider) ->
                $interpolateProvider.startSymbol('[[').endSymbol(']]')
            )
            .factory('web_socket', ['$rootScope', '$log', ($rootScope, $log) ->
                $rootScope.$log = $log
                return new pc.network.WebSocketClient($rootScope)
            ])
            .factory('channel_mgr', ['$rootScope', ($rootScope) ->
                return new pc.network.ChannelManager($rootScope)
            ])
            .factory('connection_mgr', ['$rootScope', ($rootScope) ->
                return new pc.network.ConnectionManager($rootScope)
            ])
            .factory('file_mgr', ['$rootScope', ($rootScope) ->
                return new pc.file.Manager($rootScope)
            ])
            .factory('room_mgr', ['$rootScope', ($rootScope) ->
                return new pc.room.RoomManager($rootScope)
            ])
            .factory('notifier', ['$rootScope', ($rootScope) ->
                return new pc.notification.Notifier($rootScope)
            ])
            .factory('peer_mgr', ['$rootScope', ($rootScope) ->
                return new pc.peer.PeerManager($rootScope)
            ])
            .run(['$rootScope', '$q', 'web_socket', 'peer_mgr', 'notifier', ($rootScope, $q, web_socket, peer_mgr, notifier) ->

                $rootScope.ws = web_socket
                $rootScope.peer_mgr = peer_mgr
                $rootScope.me = new pc.peer.Peer() # Temporary Peer

                $rootScope.$on "event_ws_opened", () ->
                    me = $rootScope.peer_mgr.get_peer($rootScope.ws.session_id, "Me")
                    $rootScope.me = me

                $rootScope.$q = $q
                $rootScope.notifier = notifier

                $rootScope.safe_apply = (fn, $scope) ->
                    $scope = if $scope then $scope else @
                    phase = $scope.$root.$$phase
                    if phase is '$apply' or phase is '$digest'
                        if fn and typeof(fn) is 'function'
                            fn()
                    else
                        $scope.$apply(fn)

                navigator.webkitGetUserMedia({audio: true, video: true}, (stream) ->
                    $rootScope.local_stream = stream
                    $rootScope.local_blobURL = URL.createObjectURL(stream)
                )
            ])
            .controller('ChannelController', ['$rootScope', '$scope', 'channel_mgr', pc.controller.channel_controller])
            .controller('PeerConnectionController',
                ['$rootScope', '$scope', 'connection_mgr', pc.controller.peer_connection_controller])
            .controller('RoomController', ['$rootScope', '$scope', 'room_mgr', pc.controller.room_controller])
            .controller('MainController', ['$rootScope', '$scope', pc.controller.main_controller])
            .directive('chat', pc.directive.chat)
            .directive('room', pc.directive.room)
            .directive('visio', pc.directive.visio)
            .directive('fileTransfer', pc.directive.file_transfer)
            .directive('notification', pc.directive.notification)

angular.element(document).ready () ->
    pc.init()
    angular.bootstrap(document, ['PeerCast'])
