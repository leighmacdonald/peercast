var MockWebSocketClient = (function () {
    function MockWebSocketClient() {

    }

    MockWebSocketClient.prototype.send = function (request) {
        if (request.peers == "peer1") {
            request.args.peer_id = "peer2";
        }
        else {
            request.args.peer_id = "peer1";
        }
        EventBus.dispatch_event(request.event, request.args);
    };
    return MockWebSocketClient;
})();